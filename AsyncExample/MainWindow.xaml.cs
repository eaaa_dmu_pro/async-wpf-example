﻿using AsyncExample.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AsyncExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Counter _counter;
        public MainWindow()
        {
            _counter = new Counter();
            this.DataContext = _counter;
            InitializeComponent();
        }

        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
            Thread.Sleep(5000);
        }
        private async void Btn2_Click(object sender, RoutedEventArgs e)
        {
            await Task.Delay(5000);
        }

        private void Btn_Increment_Click(object sender, RoutedEventArgs e)
        {
            _counter.Increment();
        }

    }
}
