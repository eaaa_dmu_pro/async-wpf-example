﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncExample.Model
{
    public class Counter : INotifyPropertyChanged
    {
        public int Count { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Increment()
        {
            Count++;
            Notify(nameof(Count));
        }

        private void Notify(string prop)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
